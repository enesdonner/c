#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include "transfer.h"

void writefile(int sockfd, FILE* fp)
{
    ssize_t n; //Her seferinde alınan veri sayısı
    char buff[MAX_LINE] = { 0 }; //Veri önbelleği
    while ((n = recv(sockfd, buff, MAX_LINE, 0)) > 0) {
        if (n == -1) {
            perror("Receive File Error");
            exit(1);
        }

        //Alınan verileri bir dosyaya yaz
        if (fwrite(buff, sizeof(char), n, fp) != n) {
            perror("Write File Error");
            exit(1);
        }
        memset(buff, 0, MAX_LINE); //Önbelleği boşalt
    }
}
//void writefile(int sockfd, FILE* fp);

int main(int argc, char* argv[])
{

    for (;;) {

        //Bir TCP soketi oluştur
        int sockfd = socket(AF_INET, SOCK_STREAM, 0);
        if (sockfd == -1) {
            perror("Can't allocate sockfd");
            exit(1);
        }

        //TCP soketi oluşturmak için sunucu soket adresini yapılandırma
        struct sockaddr_in clientaddr, serveraddr;
        memset(&serveraddr, 0, sizeof(serveraddr));
        serveraddr.sin_family = AF_INET;
        serveraddr.sin_addr.s_addr = htonl(INADDR_ANY);
        serveraddr.sin_port = htons(SERVERPORT);

        //Soketleri ve adresleri bağlama
        if (bind(sockfd, (const struct sockaddr*)&serveraddr, sizeof(serveraddr)) == -1) {
            perror("Bind Error");
            exit(1);
        }

        //Dinleme soketine dönüştürün
        if (listen(sockfd, LINSTENPORT) == -1) {
            perror("Listen Error");
            exit(1);
        }

        //Bağlantının tamamlanmasını bekleyin
        socklen_t addrlen = sizeof(clientaddr);
        int connfd = accept(sockfd, (struct sockaddr*)&clientaddr, &addrlen); //已连接套接字
        if (connfd == -1) {
            perror("Connect Error");
            exit(1);
        }
        close(sockfd); //Dinleme soketlerini kapatın

        //  Dosya adını al
        char filename[BUFFSIZE] = { 0 }; //文件名
        if (recv(connfd, filename, BUFFSIZE, 0) == -1) {
            perror("Can't receive filename");
            exit(1);
        }

        //Dosya oluştur
        FILE* fp = fopen(filename, "wb");
        if (fp == NULL) {
            perror("Can't open file");
            exit(1);
        }

        //Verileri dosyaya yaz

        char addr[INET_ADDRSTRLEN];
        printf("Start receive file: %s from %s\n", filename, inet_ntop(AF_INET, &clientaddr.sin_addr, addr, INET_ADDRSTRLEN));
        writefile(connfd, fp);
        puts("Receive Success");

        //Dosyaları ve bağlı yuvaları kapatın
        fclose(fp);
        close(connfd);
    }
    return 0;
}
