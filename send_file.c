#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <libgen.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <time.h>
#include <sys/stat.h>
#include <sys/types.h>
#include "transfer.h"

void sendfile(FILE* fp, int sockfd)
{
    int n; //Her seferinde okunan veri miktarı /// ÖNEMLİ
    char sendline[MAX_LINE] = { 0 }; //Geçici veriler her seferinde okunur
    while ((n = fread(sendline, sizeof(char), MAX_LINE, fp)) > 0) {
        if (n != MAX_LINE && ferror(fp)) //Okuma hatası ve dosyanın sonuna ulaşmadı
        {
            perror("Read File Error");
            exit(1);
        }

        //Okunan verileri TCP gönderme arabelleğine gönder
        if (send(sockfd, sendline, n, 0) == -1) {
            perror("Can't send file");
            exit(1);
        }
        memset(sendline, 0, MAX_LINE); //Boş çizik ip
    }
}
// void sendfile(FILE *fp, int sockfd);    // ana kod

int main(int argc, char* argv[])
{

    //Bir TCP soketi oluştur
    int sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0) {
        perror("Can't allocate sockfd");
        exit(1);
    }

    //Aktarım eşiği soket adresini ayarla
    struct sockaddr_in serveraddr;
    memset(&serveraddr, 0, sizeof(serveraddr));
    serveraddr.sin_family = AF_INET;
    serveraddr.sin_port = htons(SERVERPORT);
    //IP adresi formatını dizeden ikiliye dönüştürme          // BURAYA BAK.
    if (inet_pton(AF_INET, argv[2], &serveraddr.sin_addr) < 0) { 
        perror("IPaddress Convert Error");
        exit(1);
    }

    //Bir bağlantı kurmak
    if (connect(sockfd, (const struct sockaddr*)&serveraddr, sizeof(serveraddr)) < 0) {
        perror("Connect Error");
        exit(1);
    }
    //Göndermek istediğiniz dosyayı açın
    FILE* fp =fopen(argv[1], "rb");
    if (fp == NULL) {
        perror("Can't open file");
        exit(1);
    }

    //Dosya adını al
    char* filename = basename(argv[1]); //Dosya adı
    if (filename == NULL) {
        perror("Can't get filename");
        exit(1);
    }

    /*Dosya adını gönder
      Dosya adını bir kez göndermek için, geçici olarak TCP gönderme arabelleğinde saklamak yerine, diğer tarafın fazla veriyi almaktan kaçınması, doğru dosya adını ayrıştırmak kolay değildir.
      Tampon boyutuna gönderilecek verinin boyutunu ayarlamanız gerekiyor*/
    char buff[BUFFSIZE] = { 0 };
    strncpy(buff, filename, strlen(filename));

    if (send(sockfd, buff, BUFFSIZE, 0) == -1) {
        perror("Can't send filename");
        exit(1);
    }
    struct stat file_stats;
    stat(argv[1], &file_stats);
    char* ilk = malloc(255 * sizeof(char));
    char* son = malloc(255 * sizeof(char));
    strcpy(ilk, ctime(&file_stats.st_ctime));
    while (1) {

        struct stat file_stats1;
        stat(argv[1], &file_stats1);

        strcpy(son, ctime(&file_stats1.st_ctime));

        if (strcmp(ilk, son) != 0) {

            // Dosyaları oku ve gönder      /// ÖNEMLİ
            sendfile(fp, sockfd);
            printf("Send Success \n");
            //free(ilk);
            stat(argv[1], &file_stats);
            strcpy(ilk, ctime(&file_stats.st_ctime));
            //Dosyaları ve yuvaları kapat
            //fclose(fp);
            //close(sockfd);    
        }
        
         
    }

    return 0;
} // main sonu
